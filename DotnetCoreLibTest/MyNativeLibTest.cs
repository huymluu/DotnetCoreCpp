using DotnetCoreLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DotnetCoreLibTest
{
    [TestClass]
    public class MyNativeLibTest
    {
        [TestMethod]
        public void GetNumber()
        {
            Assert.AreEqual(2018, MyNativeLib.GetNumber());
        }
    }
}
