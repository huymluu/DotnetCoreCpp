﻿using System.Runtime.InteropServices;

namespace DotnetCoreLib
{
    public class MyNativeLib
    {
        const string DLL_PATH = "CppLibrary";

        [DllImport(DLL_PATH, EntryPoint = "MyNativeLib_getNumber")]
        public static extern int GetNumber();
    }
}
