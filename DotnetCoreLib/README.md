This is .NET Standard C# library - a wrapper for Native C++ library.

Dependencies: built binaries from `CppLibrary` Native C++ project:

- `CppLibrary.dll`
- `libCppLibrary.so`

Copy those files to `DotnetCoreLib/lib/`. Those files will by loaded by P/Invoke.

# Build into Nuget package

```
dotnet pack -c Release -o ../nupkgs
```

Output: `DotnetCoreLib.*.nupkg`

That Nuget package is just in local machine (output to `../nupkgs`), and will be consumed by `DotnetCoreCpp` console app.