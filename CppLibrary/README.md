This is native C++ lib.

We will build into binaries for Windows and Linux.

# Build for Windows

Build: use Visual Studio or docker

Output: `CppLibrary.dll`

# Build for Linux

Build: use docker cmake image: `matrim/cmake-examples`

```
docker run --rm -v ${pwd}:/data/code -it matrim/cmake-examples:3.5.1 /bin/bash -c "cd /data/code/ && chmod 777 build.sh && ./build.sh"
```

Output: `libCppLibrary.so`